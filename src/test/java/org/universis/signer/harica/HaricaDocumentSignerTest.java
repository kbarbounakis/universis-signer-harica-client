package org.universis.signer.harica;

import org.apache.commons.io.FileUtils;
import org.apache.jcp.xml.dsig.internal.dom.DOMSubTreeData;
import org.apache.poi.ooxml.util.DocumentHelper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.poifs.crypt.dsig.SignatureConfig;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo;
import org.apache.poi.poifs.crypt.dsig.facets.KeyInfoSignatureFacet;
import org.apache.poi.poifs.crypt.dsig.facets.OOXMLSignatureFacet;
import org.apache.poi.poifs.crypt.dsig.facets.Office2010SignatureFacet;
import org.apache.poi.poifs.crypt.dsig.facets.XAdESSignatureFacet;
import org.apache.poi.poifs.crypt.dsig.services.RevocationData;
import org.apache.poi.poifs.crypt.dsig.services.TimeStampService;
import org.apache.poi.util.LocaleUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

public class HaricaDocumentSignerTest {

    @org.junit.jupiter.api.BeforeAll
    public static void init() throws IOException {
        String trustStore = HaricaSignerTest.class.getResource("ssl-trust-keystore.p12").getPath();
        System.setProperty("javax.net.ssl.trustStore", trustStore);
        System.setProperty("javax.net.ssl.trustStoreType", "PKCS12");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        Properties properties = new Properties();
        properties.load( new FileInputStream("tmp/test.properties"));
        System.setProperty("harica.username", properties.getProperty("harica.username"));
        System.setProperty("harica.password", properties.getProperty("harica.password"));
        System.setProperty("harica.otp", properties.getProperty("harica.otp"));

    }

    private Element getDsigElement(Document document, String localName) {
        NodeList sigValNl = document.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", localName);
        if (sigValNl.getLength() == 1) {
            return (Element)sigValNl.item(0);
        } else {
            return null;
        }
    }

    public void sign(File inFile,
                     File outFile,
                     HaricaSigner signer,
                     String username,
                     String password,
                     String otp,
                     String reason,
                     String timestampServer) throws GeneralSecurityException, IOException, InvalidFormatException, XMLSignatureException, MarshalException, TransformException, NoSuchFieldException {
        // get cert
        HaricaSignerMessage message = new HaricaSignerMessage();
        message.username = username;
        message.password = password;
        message.otp = otp;
        HaricaCertificateMessageResult messageResult = signer.getCertificates(message);
        if (!messageResult.success) {
            throw new IOException(messageResult.error.message);
        }
        Certificate[] chain = messageResult.data.getCertificates();
        // copy file
        if (!inFile.equals(outFile)) {
            FileUtils.copyFile(inFile, outFile);
        }
        // open package
        OPCPackage pkg = OPCPackage.open(outFile, PackageAccess.READ_WRITE);
        // create signature configuration
        SignatureConfig signatureConfig = new SignatureConfig();
        // if timestamp server is not defined
        if (timestampServer == null) {
            TimeStampService tspService = new TimeStampService() {
                @Override
                public byte[] timeStamp(byte[] data, RevocationData revocationData) throws Exception {
                    return "time-stamp-token".getBytes(LocaleUtil.CHARSET_1252);
                }
                @Override
                public void setSignatureConfig(SignatureConfig config) {
                    // empty on purpose
                }
            };
            signatureConfig.setTspService(tspService);
        } else {
            // set timestamp server url
            signatureConfig.setTspUrl(timestampServer);
        }
        // set no request policy for tsp
        signatureConfig.setTspRequestPolicy(null);
        // set digest algorithm
        signatureConfig.setDigestAlgo(HashAlgorithm.sha1);
        // create certificate chain
        List<X509Certificate> certificateChain = new ArrayList<>();
        for (Certificate cert : chain) {
            certificateChain.add((X509Certificate) cert);
        }
        // set certificate chain
        signatureConfig.setSigningCertificateChain(certificateChain);

//        // set private key
          // -
        signatureConfig.setKey(new PrivateKey() {
            @Override
            public String getAlgorithm() {
                return "RSA";
            }

            @Override
            public String getFormat() {
                return null;
            }

            @Override
            public byte[] getEncoded() {
                return new byte[0];
            }
        });

        // set description
        if (reason != null) {
            signatureConfig.setSignatureDescription("Document signature");
        }
        // add signature facets
        signatureConfig.addSignatureFacet(new OOXMLSignatureFacet());
        signatureConfig.addSignatureFacet(new KeyInfoSignatureFacet());
        signatureConfig.addSignatureFacet(new XAdESSignatureFacet());
        signatureConfig.addSignatureFacet(new Office2010SignatureFacet());
        // include certificate chain
        signatureConfig.setIncludeEntireCertificateChain(true);
        // set package
        signatureConfig.setOpcPackage(pkg);
        // perform sign
        SignatureInfo si = new SignatureInfo();
        si.setSignatureConfig(signatureConfig);

        Document document = DocumentHelper.createDocument();
        DOMSignContext xmlSignContext = si.createXMLSignContext(document);
        // set provider
        Provider pkcs12 = Security.getProvider("PKCS12");
        xmlSignContext.setProperty("org.jcp.xml.dsig.internal.dom.SignatureProvider", pkcs12);
        // call pre-sign operation
        org.apache.jcp.xml.dsig.internal.dom.DOMSignedInfo signedInfo = si.preSign(xmlSignContext);
        // get signedInfo element
        Document doc = (Document)xmlSignContext.getParent();
        Element el = this.getDsigElement(doc, "SignedInfo");
        DOMSubTreeData subTree = new DOMSubTreeData(el, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // get element as stream
        signedInfo.getCanonicalizationMethod().transform(subTree, xmlSignContext, stream);
        try {
            // create signature value
            HaricaRemoteSignature remoteSignature = new HaricaRemoteSignature(signer, username, password, otp);
            // sign SignedInfo element
            byte[] res = remoteSignature.sign(stream.toByteArray());
            // get base64 value
            String signatureValue = Base64.getEncoder().encodeToString(res);
            // call post-sign operation
            si.postSign(xmlSignContext, signatureValue);
        } catch (GeneralSecurityException e) {
            throw e;
        }
        // and close document
        pkg.close();
    }


    void shouldSignDocument() throws MarshalException, XMLSignatureException, GeneralSecurityException, TransformException, IOException, NoSuchFieldException, InvalidFormatException {
        String inFile = HaricaDocumentSignerTest.class.getResource("Book1NoSignatureLine.xlsx").getPath();
        String outFile = "tmp/Book1NoSignatureLine_signed.xlsx";
        this.sign(new File(inFile),
                new File(outFile),
                new HaricaSigner("https://rsign-api-dev.harica.gr/"),
                System.getProperty("harica.username"),
                System.getProperty("harica.password"),
                System.getProperty("harica.otp"),
                null,
                null
                );
    }

}
