package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;

public class HaricaSigner {

    URI serviceURI;

    public HaricaSigner() {
        //
    }

    public HaricaSigner(String serviceURI) {
        this.serviceURI = URI.create(serviceURI);
    }

    public HaricaSigner(URI serviceURI) {
        this.serviceURI = serviceURI;
    }

    /**
     * Gets HARICA certificate collection for the given user
     * @param message - A message which contains username and password
     * @return A collection of HARICA remote certificates
     * @throws IOException
     */
    HaricaCertificateMessageResult getCertificates(HaricaSignerMessage message) throws IOException {
        // create client
        HttpClient client = HttpClientBuilder.create().build();
        // get sign endpoint
        URI signURI = URIUtils.resolve(this.serviceURI, "/dsa/v1/Certificates");
        // create request
        HttpPost request = new HttpPost(signURI);
        // get message as json string
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(message);
        // set http entity
        HttpEntity stringEntity = new StringEntity(value, ContentType.APPLICATION_JSON);
        request.setEntity(stringEntity);
        // execute request
        HttpResponse response = client.execute(request);
        if (response != null) {
            InputStream in = response.getEntity().getContent(); // get json content
            // get byte array
            byte[] inBytes = IOUtils.toByteArray(in);
            HaricaCertificateMessageResult res = new HaricaCertificateMessageResult();
            if (response.getStatusLine().getStatusCode() == 200) {
                // deserialize stream to message result
                // important: use this method because service returns also 200 on error
                HaricaCertificateMessageResult trySuccess = mapper.readValue(inBytes, HaricaCertificateMessageResult.class);
                // if success is false
                if (!trySuccess.success) {
                    // deserialize and return error
                    res.success = false;
                    res.error = mapper.readValue(inBytes, HaricaCertificateErrorResult.class);
                    return res;
                }
                // otherwise set success to true
                res.success = true;
                // and return data
                res.data = mapper.readValue(inBytes, HaricaCertificateMessageResultData.class);
            } else {
                // deserialize stream to message error result
                res.success = false;
                res.error = mapper.readValue(inBytes, HaricaCertificateErrorResult.class);
            }
            return res;

        } else {
            // throw exception for empty result
            throw new IOException("Service response cannot be empty at this context");
        }
    }


    HaricaSignBufferResult signBuffer(HaricaSignBufferMessage message) throws IOException {
        // create client
        HttpClient client = HttpClientBuilder.create().build();
        // get sign endpoint
        URI signURI = URIUtils.resolve(this.serviceURI, "/dsa/v1/SignBuffer");
        // create request
        HttpPost request = new HttpPost(signURI);
        // get message as json string
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(message);
        // set http entity
        HttpEntity stringEntity = new StringEntity(value, ContentType.APPLICATION_JSON);
        request.setEntity(stringEntity);
        // execute request
        HttpResponse response = client.execute(request);
        if (response != null) {
            InputStream in = response.getEntity().getContent(); // get json content
            // get byte array
            byte[] inBytes = IOUtils.toByteArray(in);
            HaricaSignBufferResult res = new HaricaSignBufferResult();
            if (response.getStatusLine().getStatusCode() == 200) {
                // deserialize stream to message result
                // important: use this method because service returns also 200 on error
                HaricaSignBufferResult trySuccess = mapper.readValue(inBytes, HaricaSignBufferResult.class);
                // if success is false
                if (!trySuccess.success) {
                    // deserialize and return error
                    res.success = false;
                    res.error = mapper.readValue(inBytes, HaricaSignBufferErrorResult.class);
                    return res;
                }
                res = trySuccess;
            } else {
                // deserialize stream to message error result
                res.success = false;
                res.error = mapper.readValue(inBytes, HaricaSignBufferErrorResult.class);
            }
            return res;

        } else {
            // throw exception for empty result
            throw new IOException("Service response cannot be empty at this context");
        }
    }

    HaricaSignerMessageResult sign(HaricaSignerMessage message) throws IOException {
        // create client
        HttpClient client = HttpClientBuilder.create().build();
        // get sign endpoint
        URI signURI;
        if (message.fileType.equals("pdf")) {
            signURI = URIUtils.resolve(this.serviceURI, "/dsa/v1/sign");
        } else if (message.fileType.equals("xlsx") || message.fileType.equals("docx")) {
            signURI = URIUtils.resolve(this.serviceURI, "/dsa/v1/signSigField");
        } else {
            throw new IOException("Unsupported file type");
        }
        // create request
        HttpPost request = new HttpPost(signURI);
        // get message as json string
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(message);
        // set http entity
        HttpEntity stringEntity = new StringEntity(value, ContentType.APPLICATION_JSON);
        request.setEntity(stringEntity);
        // execute request
        HttpResponse response = client.execute(request);
        if (response != null) {
            InputStream in = response.getEntity().getContent(); // get json content
             // deserialize stream to message result
            return mapper.readValue(in, HaricaSignerMessageResult.class);
        } else {
            // throw exception for empty result
            throw new IOException("Service response cannot be empty at this context");
        }
    }

    public URI getServiceURI() {
        return serviceURI;
    }

    public void setServiceURI(URI serviceURI) {
        this.serviceURI = serviceURI;
    }

    public static String tryFindSignatureLine(String file) throws InvalidFormatException, ParserConfigurationException, IOException, SAXException {
        OPCPackage xlsx = OPCPackage.open(file, PackageAccess.READ);
        String signatureLineID = null;
        ArrayList<PackagePart> parts = xlsx.getParts();
        for (PackagePart packagePart : parts) {
            if (packagePart.getContentType().equals("application/vnd.openxmlformats-officedocument.vmlDrawing")) {
                PackagePartName name = packagePart.getPartName();
                String text = IOUtils.toString(packagePart.getInputStream(), StandardCharsets.UTF_8.name());
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                InputSource is = new InputSource(new StringReader(text));
                Document doc = builder.parse(is);
                NodeList nodes = doc.getElementsByTagName("xml");
                if (nodes.getLength() > 0) {
                    // get root element
                    Element rootElement = (Element) nodes.item(0);
                    NodeList shapes = rootElement.getElementsByTagName("v:shape");
                    if (shapes.getLength() > 0) {
                        Element shape = (Element) shapes.item(0);
                        NodeList signatureLines = shape.getElementsByTagName("o:signatureline");
                        if (signatureLines.getLength() > 0) {
                            Element signatureLine = (Element) signatureLines.item(0);
                            return signatureLine.getAttribute("id");
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Signs the given input stream which represents a pdf document
     */
    HaricaSignerMessageResult sign(HaricaSignerMessage message,
                     String timestampServer,
                     String image) throws IOException, DocumentException, GeneralSecurityException {
        // create reader
        byte[] inputBuffer = Base64.getDecoder().decode(message.fileData);
        PdfReader reader = new PdfReader(new ByteArrayInputStream(inputBuffer));
        String outFile = File.createTempFile("signPDF",".pdf").getAbsolutePath();
        OutputStream outputStream = new FileOutputStream(outFile);
        // create signature
        PdfStamper stamper = PdfStamper.createSignature(reader, outputStream, '\0', null, true);
        // get signature appearance
        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        if (message.reason != null) {
            appearance.setReason(message.reason);
        }
        Font font = new Font(Font.FontFamily.HELVETICA, 10);
        appearance.setLayer2Font(font);

        // set signature position
        Rectangle finalPosition = new Rectangle(50, 10, 290, 100);
        if (message.width != null) {
            finalPosition = new Rectangle(message.x, message.y, message.width, message.height);
        }
        // add image
        if (image != null) {
            Image imageInstance = Image.getInstance(image);
            appearance.setSignatureGraphic(imageInstance);
            appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION);
        }
        appearance.setVisibleSignature(finalPosition, message.page, message.signatureFieldName == null ? "sig": message.signatureFieldName);
//        BouncyCastleProvider provider = new BouncyCastleProvider();
//        Security.addProvider(provider);
//        String providerName = provider.getName();

        HaricaCertificateMessageResult result = this.getCertificates(new HaricaSignerMessage() {
            {
                username = message.username;
                password = message.password;
            }
        });
        if (!result.success) {
            throw new IOException(result.error.message);
        }
        if (result.data.certificates.size() == 0) {
            throw new GeneralSecurityException("A valid certificate cannot be found");
        }
        // get cert
        InputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(result.data.certificates.get(0)));
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate)certFactory.generateCertificate(in);

        final Certificate[] chain = {cert};
        // create digest algorithm
        HaricaRemoteSignature signature = new HaricaRemoteSignature(this, message.username, message.password, message.otp);

        BouncyCastleDigest digest = new BouncyCastleDigest();
        TSAClientBouncyCastle tsaClient = null;
        if (timestampServer != null) {
            tsaClient = new TSAClientBouncyCastle(timestampServer, "", "");
        }
        // and finally sign pdf
        MakeSignature.signDetached(appearance, digest, signature, chain, null, null, tsaClient, 0, MakeSignature.CryptoStandard.CADES);
        outputStream.close();
        // get outfile
        byte[] outBuffer = FileUtils.readFileToByteArray( new File(outFile));
        return new HaricaSignerMessageResult() {
            {
                success = true;
                data = new HaricaSignerMessageResultData() {{
                    signedFileData = Base64.getEncoder().encodeToString(outBuffer);
                }};
            }
        };

    }



}
