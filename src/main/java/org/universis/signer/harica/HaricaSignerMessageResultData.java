package org.universis.signer.harica;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;

public class HaricaSignerMessageResultData {
    @JsonProperty("SignedFileData")
    public String signedFileData;

    public InputStream getStream() {
        if (this.signedFileData == null) {
            throw new NullPointerException("Signed data may not be null");
        }
        byte[] bytes = Base64.getDecoder().decode(this.signedFileData);
        return new ByteArrayInputStream(bytes);
    }

}
