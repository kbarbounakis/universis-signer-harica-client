package org.universis.signer.harica;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.universis.signer.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

public class HaricaOpenapiHandler extends RouterNanoHTTPD.DefaultStreamHandler {

    private static final Logger log = LogManager.getLogger(KeyStoreHandler.class);

    public HaricaOpenapiHandler() {
        //
    }

    @Override
    public String getMimeType() {
        return "application/json";
    }

    @Override
    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.OK;
    }

    @Override
    public InputStream getData() {
        return HaricaOpenapiHandler.class.getResourceAsStream("/org/universis/signer/harica/openapi.json");
    }

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> urlParams, NanoHTTPD.IHTTPSession session) {
        try {
            NanoHTTPD.Response response = super.get(uriResource, urlParams, session);
            CorsHandler.enable(response);
            return response;
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, urlParams, session);
        }

    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }
    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }
    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }

}
