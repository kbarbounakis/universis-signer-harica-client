# universis-signer-harica

![Universis Project](src/main/resources/universis_logo_128_color.png)

![HARICA](src/main/resources/harica_logo.png)

An extender of [universis signer](https://gitlab.com/universis/universis-signer) for implementing [HARICA](https://harica.gr/) cloud signing.

## Installation

1. Download [Universis signer plugin for HARICA](https://api.universis.io/releases/universis-signer-harica-client/latest/universis-signer-harica-client.zip)

2. Extract universis-signer-harica-client.zip and copy content (universis-signer-harica-client/*) to universis signer installation directory.

3. Restart universis signer

**Important note: This plugin is available for universis-signer@1.8.0 or greater**

## Manual installation

Build module 

    mvn package

Copy `target/universis-signer-harica.jar` to installation directory of universis signer

Edit `extras/service.properties` of universis signer in order to set `keyStore` and `storeType` properties

    storeType=REMOTE
    keyStore=https://rsign-api.harica.gr/

Add `extras/routes.xml` to override universis signer routes

    <routes>
        <route>
            <path>/openapi/schema.json</path>
            <plugin>universis-signer-harica.jar</plugin>
            <className>org.universis.signer.harica.HaricaOpenapiHandler</className>
        </route>
        <route>
            <path>/slots</path>
            <className>org.universis.signer.NotImplementedHandler</className>
        </route>
        <route>
            <path>/keystore/certs</path>
            <plugin>universis-signer-harica.jar</plugin>
            <className>org.universis.signer.harica.HaricaKeyStoreHandler</className>
        </route>
        <route>
            <path>/sign</path>
            <plugin>universis-signer-harica.jar</plugin>
            <className>org.universis.signer.harica.HaricaSignerHandler</className>
        </route>
    </routes>
